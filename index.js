"use strict";

const CleanCSS = require("clean-css");
const assert = require("assert");
const commonjs = require("@rollup/plugin-commonjs");
const domutils = require("domutils");
const fs = require("fs-extra");
const htmlparser2 = require('htmlparser2');
const json = require("@rollup/plugin-json");
const klaw = require("klaw");
const mime = require("mime-types");
const path = require("path");
const render = require("dom-serializer").default;
const replace = require("@rollup/plugin-replace");
const semverParse = require("semver/functions/parse");
const semverSatisfies = require("semver/functions/satisfies");
const sizeOf = require("image-size");
const { URLSearchParams } = require("url");
const { createHash } = require("crypto");
const { isMatch } = require("micromatch");
const { nodeResolve } = require("@rollup/plugin-node-resolve");
const { terser } = require("rollup-plugin-terser");

const isProduction = process.env.BUILD === "production";

function isWhitespace(el) {
    return el.type === "text" && el.data.match(/^\s+$/u);
}

function minifyHtml(htmlContent) {
    // TODO: this may not work as expected
    // use html-minifier when it is available in debian
    // https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=857108
    const dom = htmlparser2.parseDocument(htmlContent);
    for (const el of domutils.filter(isWhitespace, dom, true))
        domutils.removeElement(el);
    return render(dom);
}

function minifyCss(cssContent) {
    const options = { /* options */ };
    const cleanCSS = new CleanCSS(options); // TODO: can it be reused?
    return cleanCSS.minify(cssContent).styles; // TODO: detect errors
}

function createEntry(url, content, algorithm) {
    const hash = createHash(algorithm);
    hash.update(content);
    return { url, revision: hash.digest("hex") };
}

class WebManifest {

    #data;
    #entries;

    constructor(data) {
        ["icons", "lang", "start_url"].forEach(
            attr => assert(data[attr], `missing "${attr}" in web manifest`)
        );
        assert(Array.isArray(data.icons), "web-manifest icons must be an array");
        // https://developer.mozilla.org/en-US/docs/Web/Manifest
        data["$schema"] = "https://json.schemastore.org/web-manifest-combined.json";
        this.#data = data;
        this.#entries = {};
    }

    get entries() {
        return this.#entries;
    }

    async write(manifest, name, resourcesDir, destDir, baseUrl, algorithm) {
        // TODO: https://web.dev/maskable-icon-audit/
        const icons = [];
        const entries = [];
        const resources = {};
        const sizes = new Set();
        const iconsDir = ".";
        const resourcePrefix = "icon.";
        for (const iconPath of this.#data.icons) {
            const content = await fs.readFile(iconPath);
            const { width, height, type } = sizeOf(content);
            const size = `${width}x${height}`;
            const src = path.join(baseUrl, iconPath);
            const mimeType = mime.lookup(type);
            icons.push({ src, sizes: size, type: mimeType }); // TODO: multi-size
            resources[`icon-${size}`] = [iconPath, mimeType];
            entries.push(createEntry(src, content, algorithm));
            await manifest.addResourceFromContent(
                `icon.${size}`, content, mimeType, destDir, iconPath
            );
            sizes.add(size);
        }
        assert(sizes.has("192x192"), "missing 192x192 icon");

        for (const lang of Object.keys(this.#data.lang)) {
            const localizedDir = path.join(resourcesDir, lang);
            const data = this.#data.lang[lang];
            assert("name" in data, `missing "name" for ${lang}`);
            // TODO: test webManifest against schema
            const webManifest = Object.assign({}, this.#data, { icons, lang }, data);
            const content = JSON.stringify(webManifest);
            this.#entries[lang] = [createEntry(name, content, algorithm), ...entries];
            await fs.ensureDir(localizedDir);
            await fs.writeFile(path.join(localizedDir, name), content);
        }
    }
}

class ServiceWorker {

    #swSrc;
    #swTmp;
    #entries;

    constructor(swSrc, swTmp) {
        this.#swSrc = swSrc;
        this.#swTmp = swTmp;
        this.#entries = [];
    }

    get entries() {
        return this.#entries;
    }

    async write(manifests, webManifest, resourcesDir) {
        this.swContent = await fs.readFile(this.#swTmp, "utf8");

        const available = Object.assign(
            ...manifests.map(manifest => manifest.resourceEntries())
        );
        const selects = new Set([].concat(...manifests.map(manifest => manifest.select)));
        const moduleEntries = manifests.map(manifest => manifest.moduleEntry());

        if (webManifest) {
            for (const lang of Object.keys(webManifest.entries)) {
                const env = { lang };
                const entries = [
                    ...webManifest.entries[lang], ...this.#entries, ...moduleEntries
                ];
                for (const [resourceName, entry] of Object.entries(available)) {
                    const m = /^([^[\]]*)(\[(.*)\])?$/u.exec(resourceName);
                    if (m[3]) {
                        const params = new URLSearchParams(m[3]);
                        if (!Array.from(params.entries()).every(([k, v]) => env[k] === v))
                            continue; // resource params do not match environment
                    }
                    for (const select of selects) {
                        if (isMatch(m[1], select)) {
                            entries.push(entry);
                            break;
                        }
                    }
                }
                await this.#writeFile(path.join(resourcesDir, lang), entries);
            }
        }
        else { /* only one service worker, without localizations */
            const entries = [...this.#entries, ...moduleEntries];
            for (const [resourceName, entry] of Object.entries(available)) {
                for (const select of selects) {
                    if (isMatch(resourceName, select)) {
                        entries.push(entry);
                        break;
                    }
                }
            }
            await this.#writeFile(resourcesDir, entries);
        }
    }

    async #writeFile(destDir, entries) {
        await fs.ensureDir(destDir);
        await fs.writeFile(
            path.join(destDir, this.#swSrc),
            this.swContent.replace("self.__WB_MANIFEST", JSON.stringify(entries))
        );
    }

    getConfig(component) {
        const NODE_ENV = JSON.stringify(process.env.NODE_ENV || "production");
        return {
            input: this.#swSrc,
            output: {
                file: this.#swTmp,
                format: "esm",
                sourcemap: true
            },
            plugins: [
                replace({ "process.env.NODE_ENV": NODE_ENV, preventAssignment: true }),
                nodeResolve({ browser: true }),
                isProduction && terser()
            ]
        };
    }
}

class Manifest {

    #name;
    #version;
    #license;
    #basePath;
    #baseUrl;
    #algorithm;
    #module;
    #resources;
    #select;
    #dependencies;

    constructor( {
        name,
        version,
        license,
        basePath,
        baseUrl,
        algorithm,
        module,
        resources,
        select,
        dependencies
    }) {
        assert(name, "'name' field is required");
        assert(version, "'version' field is required");
        assert(license, "'license' field is required");
        assert(basePath, "'basePath' field is required");
        assert(baseUrl, "'baseUrl' field is required");
        assert(algorithm, "'algorithm' field is required");
        this.#name = name;
        this.#version = version;
        this.#license = license;
        this.#basePath = basePath;
        this.#baseUrl = baseUrl;
        this.#algorithm = algorithm;
        this.#module = module;
        this.#resources = resources || {};
        this.#select = new Set(select || []);
        this.#dependencies = dependencies || {};
        this.processors = {
            "text/css": minifyCss,
            "text/html": minifyHtml
        };
    }

    moduleEntry() {
        return {
            url: `${this.#baseUrl}/${this.#module.url}`,
            revision: this.#module.revision
        };
    }

    resourceEntries() {
        return Object.fromEntries(
            Object.entries(this.#resources).map(
                ([resourceName, resource]) => [
                    `${this.#name}:${resourceName}`,
                    {
                        url: `${this.#baseUrl}/${resource.url}`,
                        revision: resource.revision
                    }
                ]
            )
        );
    }

    addSelect(select) {
        assert(typeof select === "string");
        return this.#select.add(select);
    }

    get name() {
        return this.#name;
    }

    get version() {
        return this.#version;
    }

    get license() {
        return this.#license;
    }

    get basePath() {
        return this.#basePath;
    }

    get baseUrl() {
        return this.#baseUrl;
    }

    get algorithm() {
        return this.#algorithm;
    }

    get module() {
        return this.#module;
    }

    get resources() {
        return this.#resources;
    }

    get select() {
        return Array.from(this.#select);
    }

    get dependencies() {
        return this.#dependencies;
    }

    get() {
        assert(this.#module, "'module' field is not set");
        const rawManifest = {
            name: this.name,
            version: this.version,
            license: this.license,
            basePath: this.basePath,
            baseUrl: this.baseUrl,
            algorithm: this.algorithm,
            module: this.module
        };
        if (Object.keys(this.#resources).length)
            rawManifest.resources = this.#resources;
        if (this.#select.size)
            rawManifest.select = Array.from(this.#select);
        if (Object.keys(this.#resources).length)
            rawManifest.dependencies = this.dependencies;
        return rawManifest;
    }

    resolve(resourceName) {
        if (resourceName) {
            const resource = this.#resources[resourceName];
            assert(resource, `unknown resource ${this.#name}:${resourceName}`);
            return `${this.#baseUrl}/${this.#resources[resourceName].url}`;
        }
        return `${this.#baseUrl}/${this.#module.url}`;
    }

    createResource(url, content, type, preload = true) {
        return Object.assign(
            createEntry(url, content, this.#algorithm),
            { size: content.length, type, preload }
        );
    }

    addResource(name, rawContent, url, contentType) {
        const content = (
            isProduction && contentType in this.processors
                ? this.processors[contentType](rawContent)
                : rawContent
        );
        this.#resources[name] = this.createResource(url, content, contentType);
        return content;
    }

    async addResourceFromContent(name, input, contentType, destDir, resourcePath, url) {
        const output = this.addResource(name, input, url || resourcePath, contentType);
        const destPath = path.join(destDir, resourcePath);
        await fs.ensureDir(path.dirname(destPath));
        await fs.writeFile(destPath, output);
    }

    async addResourceFromFile(name, resourcePath, contentType, url) {
        const content = await fs.readFile(resourcePath, "utf8");
        return this.addResource(name, content, url || resourcePath, contentType);
    }

    async processResourceFile(name, resourcePath, contentType, destDir, url) {
        const content = await this.addResourceFromFile(name, resourcePath, contentType, url);
        const destPath = path.join(destDir, resourcePath);
        await fs.ensureDir(path.dirname(destPath));
        await fs.writeFile(destPath, content);
    }

    async processResourceFiles(pathPrefix, resourcePrefix, resources, destDir) {
        const promises = [];
        for (const [localName, [resource, contentType]] of Object.entries(resources)) {
            if (typeof resource === "string") {
                promises.push(this.processResourceFile(
                    `${resourcePrefix}${localName}`,
                    path.join(pathPrefix, resource),
                    contentType,
                    destDir
                ));
            }
            else {
                for (const [variant, resourcePath] of Object.entries(resource)) {
                    promises.push(this.processResourceFile(
                        `${resourcePrefix}${localName}[${variant}]`,
                        path.join(pathPrefix, resourcePath),
                        contentType,
                        destDir
                    ));
                }
            }
        }
        await Promise.all(promises);
    }

    async setModule(url, modulePath) {
        const content = await fs.readFile(modulePath, "utf8");
        this.#module = this.createResource(url, content, "application/javascript");
    }

    static getSelectedResources(manifests, selects) {
        const selectedResources = {};
        for (const manifest of manifests) {
            for (const resourceName of Object.keys(manifest.resources)) {
                let selection = selectedResources[manifest.name];
                if (selection && selection[resourceName])
                    continue; // already selected
                const assetName = `${manifest.name}:${resourceName}`;
                for (const select of selects) {
                    if (isMatch(assetName, select)) {
                        if (!selection)
                            selection = selectedResources[manifest.name] = {};
                        const resource = manifest.resources[resourceName];
                        const url = `${manifest.baseUrl}/${resource.url}`;
                        selection[resourceName] = url;
                        break;
                    }
                }
            }
        }
        return selectedResources;
    }
}

class Repository {

    #manifests;
    #directory;

    constructor(directory) {
        this.#manifests = {};
        this.#directory = directory;
        this.loaded = directory ? this.#load() : Promise.resolve();
    }

    get(componentName) {
        return this.#manifests[componentName];
    }

    addManifest(rawManifest) {
        const componentName = rawManifest.name;
        assert(!(componentName in this.#manifests), `manifest ${componentName} exists`);
        const manifest = new Manifest(rawManifest);
        this.#manifests[componentName] = manifest;
        return manifest;
    }

    async #loadManifest(manifestPath) {
        const rawManifest = await fs.readJson(manifestPath);
        return this.addManifest(rawManifest);
    }

    async #load() {
        const promises = [];
        await fs.ensureDir(this.#directory);
        for await (const item of klaw(this.#directory)) {
            if (item.stats.isDirectory())
                continue;
            if (item.path.endsWith(".json"))
                promises.push(this.#loadManifest(item.path));
            else
                console.warn(`${item.path} without json extension`);
        }
        return await Promise.all(promises);
    }
}

class Registry {

    #repositories;

    constructor(...repositories) {
        this.#repositories = repositories;
        this.loaded = Promise.all(this.#repositories.map(repository => repository.loaded));
    }

    resolve(importer, assetName) {
        const [componentName, resourceName] = assetName.split(":");
        for (const repository of this.#repositories) {
            const manifest = repository.get(componentName);
            if (manifest)
                return manifest.resolve(resourceName);
        }
        throw new Error(`unknown component "${componentName}" in ${importer}`);
    }

    getManifest(componentName, range) {
        for (const repository of this.#repositories) {
            const manifest = repository.get(componentName);
            if (manifest && semverSatisfies(manifest.version, range))
                return manifest;
        }
        return null;
    }

    recurseDependencies(entryManifest) {
        const manifests = [entryManifest];
        const queue = Object.entries(entryManifest.dependencies);
        while (queue.length) {
            const [name, range] = queue.pop();
            const alreadySelected = manifests.some(manifest => (
                manifest.name === name && semverSatisfies(manifest.version, range)
            ));
            if (alreadySelected)
                continue;
            const manifest = this.getManifest(name, range);
            assert(manifest, `no version of ${name} satisfies ${range}`);
            manifests.push(manifest);
            queue.push(...Object.entries(manifest.dependencies));
        }
        return manifests;
    }
}

class Element {

    #data;

    constructor(
        registry,
        manifest,
        elementJsonPath,
        destDir,
        mapsDir,
        resourcesPath,
        elementPrefix = "",
        modulePath = "index.js"
    ) {
        this.registry = registry;
        this.manifest = manifest;
        this.elementJsonPath = elementJsonPath;
        this.destDir = destDir;
        this.mapsDir = mapsDir;
        this.resourcesPath = resourcesPath;
        this.elementPrefix = elementPrefix;
        this.modulePath = modulePath;

        this.elementDir = path.dirname(this.elementJsonPath);
        this.inputPath = path.join(this.elementDir, this.modulePath);
    }

    async buildStart(options) {
        this.#data = await fs.readJson(this.elementJsonPath);

        assert(this.#data.module, `missing "module" in ${this.elementJsonPath}`);
        assert(this.#data.tagName, `missing "tagName" in ${this.elementJsonPath}`);
        this.prefix = `${this.elementPrefix}${this.#data.tagName}.`;

        await this.registry.loaded;

        if (this.#data.resources) {
            await this.manifest.processResourceFiles(
                this.elementDir, this.prefix, this.#data.resources, this.destDir
            );
        }

        if (this.#data.select) {
            const selects = new Set();
            for (const [componentName,  resourceNames] of Object.entries(this.#data.select)) {
                for (const resourceName of resourceNames) {
                    // if componentName is the empty string, use current manifest
                    let assetPrefix = `${componentName || this.manifest.name}:`;
                    if (resourceName[0] === ".") {
                        assetPrefix = `${assetPrefix}${this.prefix}`;
                        if (resourceName === ".*") { // TODO: use isMatch
                            for (const name of Object.keys(this.#data.resources))
                                selects.add(`${assetPrefix}${name}`);
                        }
                        else
                            selects.add(`${assetPrefix}${resourceName.slice(1)}`);
                    }
                    else
                        selects.add(`${assetPrefix}${resourceName}`);
                }
            }
            for (const select of selects)
                this.manifest.addSelect(select);

            const manifests = this.registry.recurseDependencies(this.manifest);
            const resources = Manifest.getSelectedResources(manifests, selects);
            const content = `export default ${JSON.stringify(resources)}`;
            await fs.writeFile(path.join(this.elementDir, this.resourcesPath), content);
        }
    }

    async writeBundle(bundle) {
        // TODO: the resource name should depend on the module name (if != index.js)
        await this.manifest.addResourceFromFile(
            `${this.prefix}index`, bundle.file, "application/javascript", this.inputPath
        );
    }

    transform(code, id) {
        if (id.endsWith(".html"))
	    return {
	        code: `export default ${JSON.stringify(minifyHtml(code))};`,
	        map: { mappings: "" }
	    };
        return null;
    }

    getConfig(component) {
        const outputPath = path.join(this.destDir, this.elementDir, this.modulePath);
        const sourcemapFile = path.join(
            this.mapsDir, this.manifest.name, this.elementDir, `${this.modulePath}.map`
        );
        return {
            input: this.inputPath,
            output: {
                file: outputPath,
                format: "esm",
                sourcemap: true,
                sourcemapFile
            },
            makeAbsoluteExternalsRelative: false,
            plugins: [
                json(),
                commonjs(),
                nodeResolve({ browser: true }),
                {
                    name: "element",
                    buildStart: options => this.buildStart(options),
                    writeBundle: bundle => this.writeBundle(bundle),
                    transform: (code, id) => this.transform(code, id)
                },
                component.resolver(),
                isProduction && terser()
            ]
        };
    }
}

class Component {

    #data;
    #imports;

    constructor(data, opts = {}) {
        assert.strictEqual(data.totates, 1, "unsupported component format");
        assert(data.module, "missing module path");
        this.#data = data;
        const {
            algorithm = "md5",
            baseUrl = "/static",
            elementPrefix = "element.",
            installDir = "dist",
            resourcesDir = "../resources",
            resourcesPath = "resources.js",
            sysManifestsDir = "/var/lib/totates/manifests",
            tmpDir = "tmp",
            webManifestPath = "manifest.json"
        } = opts;
        this.algorithm = algorithm;
        this.baseUrl = baseUrl;
        this.elementPrefix = elementPrefix;
        this.installDir = installDir;
        this.resourcesDir = resourcesDir;
        this.resourcesPath = resourcesPath;
        this.sysManifestsDir = sysManifestsDir;
        this.tmpDir = tmpDir;
        this.webManifestPath = webManifestPath;

        this.mapsDir = opts.mapsDir || path.join(installDir, "maps");
        this.staticDir = opts.staticDir || path.join(installDir, "static");
        this.manifestsDir = opts.manifestsDir || path.join(installDir, "manifests");

        const major = semverParse(this.#data.version).major;
        const componentName = `${this.#data.name}/v${major}`;
        this.basePath = path.relative(
            `${this.manifestsDir}/${componentName}`,
            `${this.staticDir}/${componentName}`
        );
        this.destDir = path.join(this.staticDir, componentName);
        if (typeof this.#data.module === "string")
            this.inputPath = this.outputPath = this.#data.module;
        else {
            this.inputPath = this.#data.module.input;
            this.outputPath = this.#data.module.output;
        }
        this.manifestPath = path.join(this.manifestsDir, `${componentName}.json`);

        this.curRepository = new Repository();
        this.devRepository = new Repository(this.manifestsDir);
        this.sysRepository = new Repository(this.sysManifestsDir);
        this.registry = new Registry(this.curRepository, this.devRepository, this.sysRepository);
        this.#imports = {};

        this.manifest = this.curRepository.addManifest({
            name: componentName,
            version: this.#data.version,
            license: this.#data.license,
            basePath: this.basePath,
            baseUrl: `${this.baseUrl}/${componentName}`,
            dependencies: this.#data.dependencies || {},
            resources: {},
            algorithm: this.algorithm
        });

        if (this.#data.elements) {
            this.elements = this.#data.elements.map(elementJsonPath => new Element(
                this.registry,
                this.manifest,
                elementJsonPath,
                this.destDir,
                this.mapsDir,
                this.resourcesPath,
                this.elementPrefix
            ));
        }

        if (this.#data.app) {
            const { favicon, indexHtml, swSrc, webManifest } = this.#data.app;
            assert(indexHtml, "missing indexHtml field");
            assert(favicon, "missing favicon field");
            if (webManifest)
                this.webManifest = new WebManifest(webManifest);
            if (swSrc) {
                const swTmp = path.join(this.tmpDir, swSrc);
                this.serviceWorker = new ServiceWorker(swSrc, swTmp);
            }
        }
    }

    async buildStart(options) {
        const { app, resources } = this.#data;
        if (resources) {
            await this.manifest.processResourceFiles("", "", resources, this.destDir);
            if (this.serviceWorker) {
                for (const [localName, [resource]] of Object.entries(resources)) {
                    const content = await fs.readFile(resource);
                    const entry = createEntry(localName, content, this.algorithm);
                    this.serviceWorker.entries.push(entry);
                }
            }
        }
        if (app) {
            await fs.ensureDir(this.resourcesDir);

            // TODO: copy favicon to static dir and substitute favicon href in html
            const rawHtml = await fs.readFile(app.indexHtml, "utf8");
            const appHtml = isProduction ? minifyHtml(rawHtml) : rawHtml;
            await fs.writeFile(path.join(this.resourcesDir, "index.html"), appHtml);

            const favicon = await fs.readFile(app.favicon);
            await fs.writeFile(path.join(this.resourcesDir, "favicon.ico"), favicon);

            if (this.serviceWorker) {
                this.serviceWorker.entries.push(
                    createEntry(".", appHtml, this.algorithm),
                    createEntry("favicon.ico", favicon, this.algorithm)
                );
            }
        }
    }

    async writeBundle(bundle) {
        await this.manifest.setModule(this.outputPath, bundle.file);
        for (const imports of Object.values(this.#imports)) {
            for (const resource of imports) {
                if (resource.indexOf(":") === -1)
                    continue;
                this.manifest.addSelect(resource);
            }
        }
        await fs.ensureDir(path.dirname(this.manifestPath));
        await fs.writeJson(this.manifestPath, this.manifest.get());
        if (this.webManifest) {
            await this.webManifest.write(
                this.manifest,
                this.webManifestPath,
                this.resourcesDir,
                this.destDir,
                this.manifest.baseUrl,
                this.algorithm
            );
        }
        if (this.serviceWorker) {
            const manifests = this.registry.recurseDependencies(this.manifest);
            await this.serviceWorker.write(
                manifests, this.webManifest, this.resourcesDir
            );
        }
    }

    resource(name, contentType) {
        return {
            name: "resource",
            writeBundle: async bundle => {
                const url = path.relative(this.destDir, bundle.file);
                await this.manifest.addResourceFromFile(name, bundle.file, contentType, url);
            }
        };
    }

    resolver(translations) {
        // TODO: should be static fields when eslint understands them
        const RELATIVE_PREFIX = "relative:";
        const EXTERNAL_PREFIX = "external:";
        const imports = new Set();
        return {
            name: "resolver",
            buildStart: options => {
                assert.strictEqual(options.input.length, 1);
                this.#imports[options.input[0]] = imports;
            },
            resolveId: (importee, importer, resolveOptions) => {
                if (translations && importee in translations)
                    importee = translations[importee];
                if (importee.startsWith(EXTERNAL_PREFIX)) {
                    const assetName = importee.slice(EXTERNAL_PREFIX.length);
                    const id = this.registry.resolve(importer, assetName);
                    if (id) {
                        imports.add(assetName);
                        return { id, external: true };
                    }
                }
                return null;
            }
        };
    }

    getConfig() {
        const sourcemapFile = path.join(
            this.mapsDir, this.manifest.name, `${this.inputPath}.map`
        );
        const componentConfig = {
            input: this.inputPath,
            output: {
                file: path.join(this.destDir, this.outputPath),
                format: "esm",
                sourcemap: true,
                sourcemapFile
            },
            makeAbsoluteExternalsRelative: false,
            plugins: [
                json(),
                commonjs(),
                nodeResolve({ browser: true }),
                this.resolver(),
                {
                    name: "component",
                    buildStart: options => this.buildStart(options),
                    writeBundle: bundle => this.writeBundle(bundle)
                },
                isProduction && terser()
            ]
        };

        if (!this.elements && !this.serviceWorker)
            return componentConfig;

        const config = [componentConfig];
        if (this.elements)
            config.unshift(...this.elements.map(element => element.getConfig(this)));
        if (this.serviceWorker)
            config.unshift(this.serviceWorker.getConfig(this));
        return config;
    }
}

module.exports = {
    Component,
    Element,
    Manifest,
    Registry,
    ServiceWorker,
    WebManifest,
    createEntry,
    minifyCss,
    minifyHtml
};
